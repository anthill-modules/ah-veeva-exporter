const fs = require('fs');
const Path = require('path');
const gulp = require('gulp');
const del = require('del');
const template = require('gulp-template');
const replace = require('gulp-replace');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const stylus = require("gulp-stylus");
const gulpif = require("gulp-if");

const exporter = require('ah-content-exporter');

module.exports = function (slides, config,next) {
    var path = config.path || 'app/slides/';
    var outputPath = config.output || 'exports';
    var prefix = config.prefix || '';

    var slidesPath = Path.resolve(process.cwd(), path);

    var jsonTemplate = require('../framework-presenatation.json');

    var startTime = Date.now();
    var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
    var destFolder = Path.resolve(outputPath, 'veeva-export-' + now);
    var dest = Path.join(destFolder, 'tmp');

    var buildSettings = require(process.cwd() + '/templates/veeva-wide-shared/build.json');

    var endTime, diffTime;
    var ln = slides.length;

    function copyFromTemplate() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(process.cwd() + '/templates/veeva-wide-slide/**/*')
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        addSharedFolder();
                    }
                });
        });
    }

    // TODO: run next four as parallell instead of serial
    function addSharedFolder() {
        gulp.src(buildSettings.copies)
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                addSharedJS();
            });
    }

    function addSharedJS() {
        gulp.src(buildSettings.sharedJS)
            .pipe(concat('bundle.js'))
            .pipe(uglify())
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                addSharedCSS();
            });
    }

    function addSharedCSS() {
        gulp.src(buildSettings.sharedCSS)
        // .pipe(gulpif(/[.]styl$/, stylus()))
            .pipe(replace(/[\w-_\/\.]+(\/module_assets\/[\w-_\/]+\.(jpg|png|woff|otf))/g, '.$1'))
            .pipe(replace(/[\w-_\/\.]+(\/assets\/images\/[\w-_\/]+\.(jpg|png))/g, '.$1'))
            .pipe(replace(/[\w-_\/\.]+(\/thumbnails\/[\w-_\/]+\.(jpg|png))/g, '.$1'))
            .pipe(replace(/\@font-face\s*\{[\s\w\n-:;]*\}/g, '')) // we'll load custom fonts separately
            .pipe(concat('bundle.css'))
            .pipe(cleanCSS())
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                getSlideFiles();
            });
    }

    function resolveDependencies(slide, isOnlyHTML) {
        var local = require(Path.join(process.cwd(), 'app/slides/' + slide + '/dependencies.json'));
        var deps = [];
        if (local.popups) {
            local.popups.forEach(function(popup) {
                if(isOnlyHTML){
                    deps.push('build/agnitio/slides/' + popup + ".html");
                }else{
                    deps.push('build/agnitio/**/' + popup + '*');
                }
            });
        }
        return deps;
    }

    function addSharedThumbsFolder() {
        var counter = 0;
        slides.map(function(slide) {
            console.log(Path.join(process.cwd(), 'build/agnitio/content/thumbnails/*'));
            console.log(destFolder + '/' + prefix + slide + '/shared/thumbnails');
            gulp.src(Path.join(process.cwd(), 'build/agnitio/content/thumbnails/*'))
                .pipe(gulp.dest(destFolder + '/' + prefix + slide + '/shared/thumbnails'))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        console.log("done");
                        getSlideDependeciesFiles(true);
                    }
                });
        });
    }

    function getSlideDependeciesFiles() {
        var counter = 0;
        slides.map(function(slide) {
            var dependencies = resolveDependencies(slide, true);
            gulp.src(dependencies)
                .pipe(gulp.dest(destFolder + '/' + prefix + slide + '/slides'))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        replacePaths(true);
                    }
                });
        });
    }

    function getSlideFiles() {
        var counter = 0;
        slides.map(function(slide) {
            var dependencies = resolveDependencies(slide).concat(['build/agnitio/**/' + slide + '*']);
            gulp.src(dependencies)
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        insertSlideHTML();
                    }
                });
        });
    }

    function insertSlideHTML() {
        var counter = 0;
        slides.map(function(slide) {
            var index = dest + '/' + prefix + slide + '/index.html';
            var src = dest + '/' + prefix + slide + '/slides/' + slide + '.html';
            fs.readFile(src, 'utf8', function(err, slideHtml) {
                if (!err) {
                    fs.readFile(index, 'utf8', function(err, indexHtml) {
                        var updated = indexHtml.replace(/<!-- slideHtml -->/, slideHtml);
                        fs.writeFile(index, updated, 'utf8', function(err) {
                            if (!err) {
                                counter += 1;
                                if (counter >= ln) {
                                    createStoryboard();
                                }
                            }
                            else {
                                next(err);
                            }
                        });
                    });
                }
                else{
                    // We just ignore then
                    counter += 1;
                    if (counter >= ln) {
                        createStoryboard();
                    }

                }
            });
        });
    }

    function createStoryboard() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*.json')
                .pipe(template({slideId: slide}))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        buildCSS();
                    }
                });
        });
    }

    function buildCSS() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*css')
                .pipe(concat('slide.css'))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        buildJS();
                    }
                });
        });
    }

    function buildJS() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*js')
                .pipe(concat('slide.js'))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        replacePaths(false);
                    }
                });
        });
    }

    // Replace local paths to point to shared folder
    function replacePaths(isDependecies) {
        var counter = 0,
            filePath = isDependecies ? '/slides/*' : '/**/*',
            destPath = isDependecies ? destFolder : dest,
            savedPath = isDependecies ? '/slides/' : '';

        slides.map(function(slide) {
            gulp.src(destPath + '/' + prefix + slide + filePath)
                .pipe(replace(/\.\.\/shared/g, 'shared'))
                .pipe(replace(/[\w-_\/\.]+(\/module_assets\/[\w-_\/]+\/images\/[\w-_\/]+\.(jpg|png))/g, 'shared$1'))
                .pipe(replace(/[\w-_\/\.]+(\/assets\/images\/[\w-_\/]+\.(jpg|png))/g, 'shared$1'))
                .pipe(replace(/[\w-_\/\.]+(\/thumbnails\/[\w-_\/]+\.(jpg|png))/g, 'shared$1'))
                .pipe(gulp.dest(destPath + '/' + prefix + slide + savedPath))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        if(isDependecies) {
                            copySharedJSON();
                        }else{
                            copySharedIntoSlides();
                        }
                    }
                });
        });
    }

    // Copy shared into each slide
    function copySharedIntoSlides() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/shared/**/*')
                .pipe(gulp.dest(dest + '/' + prefix + slide + '/shared'))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        // endTime = Date.now();
                        // diffTime = (endTime - startTime) / 1000;
                        // console.log("Project exported to " + dest + " in " + diffTime + " seconds.");
                        // next(null, dest);
                        finalExport();
                    }
                });
        });
    }

    // Use content exporter module to get only the files relevant to the slide
    function finalExport() {
        var counter = 0;
        slides.map(function(slide) {
            var sPath = Path.join(dest, prefix + slide, 'index.html');
            exporter.run(sPath, {
                output: prefix + slide,
                dest: '../../',
                include: ['presentation.json']
            }, function(err, res) {
                counter += 1;
                if (counter >= ln) {
                    addSharedThumbsFolder();
                }
            });
        });
    }

    // Copy json file back to slides. A bit stupid but can change later
    function copySharedJSON() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(process.cwd() + '/templates/veeva-wide-shared/**/*.json')
                .pipe(gulp.dest(destFolder + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        endTime = Date.now();
                        diffTime = (endTime - startTime) / 1000;
                        console.log("Project exported to " + destFolder + " in " + diffTime + " seconds.");
                        del([dest]).then(function () {
                            next(null, destFolder);
                        })
                    }
                });
        });
    }
    copyFromTemplate();

};
