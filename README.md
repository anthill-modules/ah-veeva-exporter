# Anthill Veeva Exporter

Export presentations targeted at Veeva platform.

# version 0.7.6
Fixed problem with generation slides on old fusion engine for exports into veeva wide mode.
Was updated method finalExport - file presentation.json will be copy with index.html from tmp folder in exports slide.
Was created method for copy slides folder from tmp with popups templates, method copyPopupsSlidesData.