const fs = require('fs-extra');
const Path = require('path');
const gulp = require('gulp');

function addSitemapThumbs(next, savingFunc) {
	console.log('START: Add thumb Sitemap');
	var src = Path.join(process.cwd(), 'build/shared/thumbnails');
	if(fs.existsSync(src)){
		src = Path.join(src, '**/*');
		savingFunc(src, next);
	} else {
		console.log('END FAIL: Add thumb Sitemap');
		next();
	}
}
module.exports = addSitemapThumbs;
