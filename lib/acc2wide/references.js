const commonModule = require('./common');
const getJsonData = commonModule.getJsonData;

function updateReferencesNumbers(slides, destFolder, ln, separator) {
	var updateSlidesData = commonModule.updateSlidesData(slides, destFolder, ln);

	return function(next) {
		console.log('START: References update');
		var result = getListOfSlides(slides),
			refs = getListOfReferences(result.slideshows);

		function parseReferences($, slide) {
			var refsOnSlide = $('[data-reference-id]');
			refsOnSlide.each(function (index, refNode) {
				var attr = $(refNode).attr('data-reference-id');
				var key = attr.split(',').map(function (refCode) {
					return refCode.split('-')
						.map(function (refKey) {
							return refs[result.slides[slide.name]].indexOf(refKey) + 1;
						})
						.join(separator);
				})
					.join(', ');
				$(refNode).text(key);
			});
			return $;
		}

		updateSlidesData(parseReferences, 'References', next);
	}
}

function getListOfSlides(slides) {
	console.log('Get list of slides');
	var result = {slides: {}, slideshows: []};
	slides.forEach(function (slide) {
		if (result.slideshows.indexOf(slide.storyboard) === -1) {
			result.slideshows.push(slide.storyboard);
		}
		result.slides[slide.name] = slide.storyboard;
	});
	return result;
}

function getListOfReferences(collections) {
	console.log('Get list of refs');
	var refs = {};
	var data = getJsonData({path: '/build/', name: 'media.json'});
	Object.keys(data).forEach(function (ref) {
		collections.forEach(function (collection) {
			if (!refs[collection]) {
				refs[collection] = [];
			}
			if (!data[ref].flow || data[ref].flow.indexOf(collection) > -1) {
				refs[collection].push(data[ref].referenceId);
			}
		});
	});

	return refs;
}

module.exports = updateReferencesNumbers;
