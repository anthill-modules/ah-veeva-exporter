const fs = require('fs');
const Path = require('path');
const gulp = require('gulp');
const del = require('del');
const template = require('gulp-template');
const gulpif = require('gulp-if');
const replace = require('gulp-replace');
const series = require('run-sequence');
const merge = require('merge-stream');
const coffee = require('gulp-coffee');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const concat = require('gulp-concat');
const jsonMerge = require('gulp-merge-json');
const fileInsert = require('gulp-file-insert');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const autoprefix = require('gulp-autoprefixer');

const acc2wideExternal = require('./lib/acc2wide-external-shared');
const acc2wideBundled = require('./lib/acc2wide-bundled-shared');
const toVeevaWideBundled = require('./lib/toVeevaWide-bundled-shared');
const old2WideBundled = require('./lib/old2Wide-bundled-shared');
const old2WideExternal = require('./lib/old2Wide-external-shared');


module.exports = {
    acceleratorToWide: acc2wideExternal,
    // Second version that bakes shared folder into each slide
    acceleratorToWide2: acc2wideBundled,
    // More simple version without font magic
    toVeevaWideBundled: toVeevaWideBundled,
    // Currently only works when running from root in a Fusion project
    oldToWide: old2WideBundled,
    oldToWide2: old2WideExternal
    // oldToWide: function veeva(slides, config, next) {
    //     var path = config.path || 'app/slides/';
    //     var outputPath = config.output || 'exports';
    //     var prefix = config.prefix || '';

    //     var slidesPath = Path.resolve(process.cwd(), path);

    //     var jsonTemplate = require('./framework-presenatation.json');

    //     var startTime = Date.now();
    //     var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
    //     var dest = Path.resolve(outputPath, 'veeva-export-' + now);

    //     // Loop through each slide folder
    //     // Get source of build
    //     // Replace storyboard in presentation.json

    //     gulp.task('export', function(done) {
    //         // var slides = getFolders(slidesPath);
    //         var endTime, diffTime;
    //         var counter = 0;
    //         var ln = slides.length;

    //         // gulp.src(Path.join(slidesPath, slide, '/**/*'))
    //         slides.map(function(slide) {
    //             gulp.src('build/agnitio/**/*')
    //                 .pipe(gulpif(/presentation.json/, replace(/[.\s\S]*/, JSON.stringify(jsonTemplate, null, 4))))
    //                 .pipe(gulpif(/presentation.json/, template({slideId: slide})))
    //                 // .pipe(zip(slide + '.zip'))
    //                 .pipe(gulp.dest(dest + '/' + prefix + slide))
    //                 .on('end', function() {
    //                     counter += 1;
    //                     if (counter >= ln) {
    //                         done();
    //                         endTime = Date.now();
    //                         diffTime = (endTime - startTime) / 1000;
    //                         console.log("Project exported to " + dest + " in " + diffTime + " seconds.");
    //                         next(null, dest);
    //                     }
    //                 });
    //         });
    //     });

    //     gulp.start.apply(gulp, ['export']);
    // }
};
