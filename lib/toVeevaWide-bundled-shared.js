const fs = require('fs-extra');
const Path = require('path');
const gulp = require('gulp');
const del = require('del');
const template = require('gulp-template');
const gulpif = require('gulp-if');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const merge = require('merge-stream');
const coffee = require('gulp-coffee');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const header = require('gulp-header');
const cheerio = require('cheerio');

const exporter = require('ah-content-exporter');

// Utility functions

function getPresentationSlides(next) {
    var path = Path.resolve(process.cwd(), 'app/platforms/rainmaker/presentation.json');
    var slides = [];
    console.log('Getting slides from presentation.json');
    fs.readFile(path, 'utf8', function (err, data) {
        if (!err) {
            var model = JSON.parse(data);
            var storyboards = Object.keys(model.storyboards);
            storyboards.forEach(function (storyboard) {
                model.storyboards[storyboard].content.forEach(function (structure) {
                    model.structures[structure].content.forEach(function (slide) {
                        slides.push({name: slide, id: structure + '_' + slide, storyboard: storyboard, structure: structure});
                    });
                });
            });
            next(slides);
        }
        else {
            console.log('Couldn\'t read presentation.json file');
            next(slides);
        }
    });
}

function buildCache(next) {
    var path = Path.resolve(process.cwd(), 'app/platforms/rainmaker/presentation.json');
    console.log('Building cache file');
    fs.readFile(path, 'utf8', function (err, data) {
        // var escaped = JSON.stringify(JSON.parse(data));
        var str = 'app.cache.put(\'package.json\',\'\')';
        var file = Path.join(process.cwd(), 'app/platforms/veeva/js/cache.js');
        if (!err) {
            fs.writeFile(file, str, function (err) {
                if (err) console.log('Error found writing media file:', err, __dirname);
                next(err);
            });
        }
        else {
            console.log('Couldn\'t read presentation.json file');
            next();
        }
    });
}

module.exports = function (slides, config, cb) {
    // getPresentationSlides(function(slides) {
    console.log('Beginning conversion to Veeva Wide');
    console.log('NUMBER OF SLIDES:', slides.length);
    buildCache(function (err) {
        var path = config.path || 'app/slides/';
        var outputPath = config.output || 'exports';
        var prefix = config.prefix || '';
        var cleanup = config.cleanup || false; // Clean up slides not linked to
        var references = config.references || false; // Clean up slides not linked to
        var limited = config.limited || false; // Limit overview to structure

        var slidesPath = Path.resolve(process.cwd(), path);

        // var startTime = Date.now();
        var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
        // var dest = Path.resolve(outputPath, 'veeva-export-' + now);
        var destFolder = Path.resolve(outputPath, 'veeva-export-' + now);
        var dest = Path.join(destFolder, 'tmp');

        //  var buildSettings = require(process.cwd() + '/templates/veeva-wide-shared/build.json');

        // var endTime, diffTime;
        var ln = slides.length;

        function addVeevaWideJS(next) {
            console.log('Adding Veeva Wide JS files');
            var src = process.cwd() + '/app/platforms/veeva/js/**/*';
            gulp.src(src)
                .pipe(gulp.dest(dest + '/shared'))
                .on('end', function () {
                    next();
                });
        }

        function updateModelJSON(next) {
            console.log('Update model JSON in builder folder');
            var src = process.cwd() + '/build/config.json';

            var counter = 0;
            fs.readFile(src, 'utf8', function (err, data) {
                if (!err) {
                    var model = JSON.parse(data);
                    model.model = 'model.json';

                    fs.writeFile(src, JSON.stringify(model, null, 2), 'utf8', function (err) {
                        if (!err) {
                            counter += 1;
                            if (counter >= ln) {
                                next();
                            }
                        }
                        else {
                            next(err);
                        }
                    });
                }
            });

            gulp.src(src)
                .pipe(gulp.dest(dest + '/shared'))
                .on('end', function () {
                    next();
                });
        }

        function addSharedAssets(next) {
            console.log('Adding shared assets');
            var src = [process.cwd() + '/app/shared/**/*', '!' + process.cwd() + '/app/shared/images/icons/*', '!' + process.cwd() + '/app/shared/pdfs/*'];
            gulp.src(src)
                .pipe(gulp.dest(dest + '/shared/shared'))
                .on('end', function () {
                    next();
                });
        }

        function addPresentationData(next) {
            console.log('Adding presentation.json');
            var src = process.cwd() + '/app/platforms/rainmaker/presentation.json';
            fs.readFile(src, 'utf8', function (err, data) {
                // Need to replace paths and file types
                // ../shared/content/rainmaker_modules/
                data = data
                // .replace(/"slides\//g, '"shared/content/')
                    .replace(/"modules\//g, '"')
                    .replace(/\.coffee/g, '.js')
                    .replace(/\.styl/g, '.css')
                    .replace(/\.pug/g, '.html')
                    .replace(/\.jade/g, '.html');

                fs.writeFile(dest + '/shared/presentation.json', data, function (err) {
                    if (err) console.log('Error while writing presentation.json', err);
                    next();
                });
            });
        }


        function getListOfSlides() {
            console.log('Get list of slides');
            var result = {slides: {}, slideshows: []};
            slides.forEach(function (slide) {
                if (result.slideshows.indexOf(slide.storyboard) === -1) {
                    result.slideshows.push(slide.storyboard);
                }
                result.slides[slide.name] = slide.storyboard;
            });
            return result;
        }

        function getListOfReferences(collections) {
            console.log('Get list of refs');
            var refs = {};
            var src = process.cwd() + '/build/media.json';
            var data = fs.readJsonSync(src);
            Object.keys(data).forEach(function (ref) {
                collections.forEach(function (collection) {
                    if (!refs[collection]) {
                        refs[collection] = [];
                    }
                    if (data[ref].flow.indexOf(collection) > -1) {
                        refs[collection].push(data[ref].referenceId);
                    }
                });
            });

            return refs;
        }

        function updateReferencesNumbers(next) {
            console.log('References updating!');
            var result = getListOfSlides();
            var refs = getListOfReferences(result.slideshows);
            var counter = 0;

            slides.map(function (slide) {
                var slideId = slide.id;
                var slidePath = Path.join(dest, slideId, 'index.html');

                fs.readFile(slidePath, 'utf8', function (err, slideHtml) {
                    if (!err) {
                        const $ = cheerio.load(slideHtml);
                        var refsOnSlide = $('[data-reference-id]');
                        refsOnSlide.each(function(index, refNode){
                            var attr = $(refNode).attr('data-reference-id');
                            var key = attr.split(',').map(function (refCode) {
                                return refCode.split('-')
                                    .map(function (refKey) {
                                        return refs[result.slides[slide.name]].indexOf(refKey) + 1;
                                    })
                                    .join('-');
                            })
                                .join(',');
                            $(refNode).text(key);
                        });

                        fs.outputFile(slidePath, $.html(), 'utf8', function (err) {
                            if (!err) {
                                counter += 1;
                                if (counter >= ln) {
                                    console.log('References updated!');
                                    next();
                                }
                            }
                            else {
                                console.log(err);
                                next(err);
                            }
                        });
                    }
                    else {
                        // We just ignore then
                        counter += 1;
                        if (counter >= ln) {
                            console.log('References updated!')
                            next();
                        }

                    }
                });
            });

        }

        function addBuildStyles(next) {
            console.log('Adding build styles');
            var pathToStyles = process.cwd() + '/build/build/presentation/**/*';
            gulp.src(pathToStyles)
                .pipe(gulp.dest(dest + '/shared/build/presentation'))
                .on('end', function () {
                    console.log('Added build styles');
                    next();
                });
        }

        function addModulesAssets(next) {
            console.log('Adding modules assets');
            var pathToStyles = process.cwd() + '/app/modules/**/*.{png,jpg,jpeg}';
            gulp.src(pathToStyles)
                .pipe(gulp.dest(dest + '/shared/modules'))
                .on('end', function () {
                    next();
                });
        }

        // Writes to final export (destFolder)
        function updateSlideModel(next) {
            console.log('Updating slide model');
            var src = Path.join(process.cwd(), 'build/presentation.json');
            var counter = 0;
            fs.readFile(src, 'utf8', function (err, data) {
                if (!err) {
                    slides.map(function (slide) {
                        var model = JSON.parse(data);
                        var file = Path.join(destFolder, slide.id, 'model.json');
                        // eg Xarelto, change structure to have single slide
                        if (!prefix || limited) {
                            model.structures.overview = {
                                'name': 'Overview',
                                'id': 'overview',
                                'type': 'slideshow',
                                'content': model.structures[slide.structure].content.slice()
                            };
                            model.structures[slide.structure].content = [slide.name];
                            model.storyboards[slide.storyboard].content = [slide.structure];
                        }
                        // eg Mirena, change storyboard to have single chapter (that has single slide)
                        else {
                            model.structures.overview = {
                                'name': 'Overview',
                                'id': 'overview',
                                'type': 'slideshow',
                                'content': model.storyboards[slide.storyboard].content.slice()
                            };
                            model.storyboards[slide.storyboard].content = [slide.structure];
                        }
                        fs.writeFile(file, JSON.stringify(model, null, 2), 'utf8', function (err) {
                            if (!err) {
                                counter += 1;
                                if (counter >= ln) {
                                    next();
                                }
                            }
                            else {
                                next(err);
                            }
                        });
                    });
                }
            });
        }

        function insertSlideHTML(next) {
            console.log('Insert slide HTML');
            var counter = 0;
            var index = Path.join(process.cwd(), 'build', 'index.html');
            var head = Path.join(process.cwd(), 'app/platforms/veeva', 'head.html');
            var bottom = Path.join(process.cwd(), 'app/platforms/veeva', 'bottom.html');

            fs.readFile(index, 'utf8', function (err, indexHtml) {
                fs.readFile(head, 'utf8', function (err, headHtml) {
                    fs.readFile(bottom, 'utf8', function (err, bottomHtml) {
                        slides.map(function (slide) {
                            // console.log(slide);
                            var slideId = slide.id;
                            if (prefix) slideId = prefix + slide.name;
                            // var index = dest + '/' + slideId + '/index.html';

                            var destPath = Path.join(dest, slideId, 'index.html');
                            var src = Path.join(process.cwd(), 'build', 'slides', slide.name, slide.name + '.html');
                            var slideAssetsPath = '"slides/' + slide.name + '/assets/';
                            fs.readFile(src, 'utf8', function (err, slideHtml) {
                                if (!err) {
                                    var updated = indexHtml.replace(/<!--slideHtml-->/, slideHtml);
                                    updated = updated.replace('class="slide"', 'class="slide present"');
                                    updated = updated.replace('class="slide ', 'class="slide present ');
                                    updated = updated.replace(/<!--headHTML-->/g, headHtml);
                                    updated = updated.replace(/<!--bottomHTML-->/g, bottomHtml);
                                    updated = updated.replace(/<%= slide %>/g, slide.name);
                                    updated = updated.replace(/<%= slideshow %>/g, slide.structure);
                                    updated = updated.replace(/%--slideshow--%/g, slide.structure);
                                    updated = updated.replace(/%--typeBuild--%/g, 'veeva-wide');
                                    updated = updated.replace(/<%= storyboard %>/g, slide.storyboard);
                                    updated = updated.replace(/<%= prefix %>/g, prefix);
                                    updated = updated.replace(/"assets\//g, slideAssetsPath);
                                    fs.outputFile(destPath, updated, 'utf8', function (err) {
                                        if (!err) {
                                            counter += 1;
                                            if (counter >= ln) {
                                                next();
                                            }
                                        }
                                        else {
                                            console.log(err);
                                            next(err);
                                        }
                                    });
                                }
                                else {
                                    // We just ignore then
                                    counter += 1;
                                    if (counter >= ln) {
                                        next();
                                    }

                                }
                            });
                        });
                    });
                });
            });
        }

        function addSlideAssets(next) {
            console.log('Add slide assets');
            var counter = 0;
            slides.map(function (slide) {
                var folder = dest + '/' + slide.id + '/slides/' + slide.name + '/assets';
                var src = Path.join(process.cwd(), 'build', 'slides', slide.name, 'assets/**/*');
                gulp.src(src)
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        function addSlidePopupAssets(next) {
            console.log('Add slide popup assets');
            var counter = 0;
            slides.map(function (slide) {
                var folder = dest + '/' + slide.id + '/slides';
                var src = [Path.join(process.cwd(), 'build', 'slides/**PopupSlide/assets/**/*'), Path.join(process.cwd(), 'build', 'slides/**AlternativeSlide/assets/**/*')]
                gulp.src(src)
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }


        /*  function deleteNonPopupSlides(next) {
         console.log('Deleting non-popup slides');
         var src = Path.join(dest, 'slides/!**!/!*');
         // var src = Path.join(dest, 'shared/slides/!*');
         // var notAssets = '!' + src + '/assets';
         // var notAssetsContent = '!' + src + '/assets/!**!/!*';
         var notPopups = '!' + src + 'PopupSlide';
         var notAlternativeSlide = '!' + src + 'AlternativeSlide';
         var notPopupsContent = notPopups + '/!**!/!*';
         var notAlternativeSlideContent = notAlternativeSlide + '/!**!/!*';
         // var modulesSrc = Path.join(dest, 'modules/rainmaker_modules');
         // var notModules = '!' + modulesSrc;
         // var notModulesContent = notModules + '/!**!/!*';
         del([src, notPopups, notPopupsContent, notAlternativeSlide, notAlternativeSlideContent]).then(next);
         }
         */
        function deleteRedundantFiles(next) {
            console.log('Deleting redundant files');
            var slideHtml = Path.join(dest, 'slides/**/*.html');
            var slideCss = Path.join(dest, 'slides/**/*.css');
            var slideJs = Path.join(dest, 'slides/**/*.js');
            // var slideJson = Path.join(dest, 'shared/slides/**/*.json');
            var moduleCss = Path.join(dest, 'modules/**/*.css');
            var moduleJs = Path.join(dest, 'modules/**/*.js');
            var presentationFile = Path.join(dest, 'presentation.json');
            del([slideHtml, slideCss, slideJs, moduleCss, moduleJs, presentationFile]).then(next);
        }

        function moveSharedPdfs(next) {
            console.log('Moving shared PDFs');
            var src = process.cwd() + '/build/shared/pdfs/**/*.pdf';
            var folder = Path.resolve(dest, '../../shared_pdfs');
            del([folder]).then(function () {
                gulp.src(src)
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        next(null, dest);
                    });
            });
        }

        // Copy the shared folder into each exported slide
        function insertSharedFolder(next) {
            console.log('Insert shared folder');
            var counter = 0;
            slides.map(function (slide) {
                var folder = dest + '/' + slide.id;
                // var src = Path.join(process.cwd(), 'build', 'slides', slide.name, '**/*.png');
                var src = Path.join(dest, 'shared/**/*');
                gulp.src(src)
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        // Use content exporter module to get only the files relevant to the slide
        function finalExport(next) {
            console.log('Remove unnecessary files');
            var counter = 0;
            slides.map(function (slide) {
                var sPath = Path.join(dest, slide.id, 'index.html');
                exporter.run(sPath, {
                    output: slide.id,
                    dest: '../../',
                    include: ['modules/rainmaker_modules/ah-notepad/assets/brush_red.png']
                }, function (err, res) {
                    if (err) console.log(err);
                    counter += 1;
                    if (counter >= ln) {
                        next();
                    }
                });
            });
        }

        function addSlideThumbs(next) {
            console.log('Add slide thumbs');
            var counter = 0;
            slides.map(function (slide) {
                var folder = Path.join(destFolder, slide.id);
                var src = Path.join(process.cwd(), 'app/slides', slide.name, 'thumbnails/full-veeva.png');
                gulp.src(src)
                    .pipe(rename('thumb.png'))
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        function addVendorScripts(next) {
            console.log('Add unreferenced files');
            var counter = 0;
            slides.map(function (slide) {
                var folder = Path.join(destFolder, slide.id, '_vendor');
                // var src = Path.join(process.cwd(), 'build', 'slides', slide.name, '**/*.png');
                var src = Path.join(process.cwd(), 'app/_vendor/**/*');
                gulp.src(src)
                    // .pipe(gulpif(/^((?!min\.js).)*$/, uglify()))
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        function addLibFiles(next) {
            console.log('Add lib files');
            var counter = 0;
            slides.map(function (slide) {
                var folder = Path.join(destFolder, slide.id, 'accelerator/lib');
                // var src = Path.join(process.cwd(), 'build', 'slides', slide.name, '**/*.png');
                var src = Path.join(process.cwd(), 'app/accelerator/lib/**/*');
                gulp.src(src)
                    // .pipe(gulpif(/^((?!min\.js).)*$/, uglify()))
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        function addJsonFiles(next) {
            console.log('Add JSON files');
            var counter = 0;
            slides.map(function (slide) {
                var folder = Path.join(destFolder, slide.id);
                // var src = Path.join(process.cwd(), 'build', 'slides', slide.name, '**/*.png');
                var src = [];
                src.push(Path.join(process.cwd(), 'build', '**/*.json'));
                src.push('!' + Path.join(process.cwd(), 'build', '**/*-strings.json'));
                src.push('!' + Path.join(process.cwd(), 'build/{slides,platforms}/**/*.json'));
                src.push('!' + Path.join(process.cwd(), 'build/modules/**/{model,package}.json'));
                gulp.src(src)
                    .pipe(gulp.dest(folder))
                    .on('end', function () {
                        counter += 1;
                        if (counter >= ln) {
                            next(null, dest);
                        }
                    });
            });
        }

        function runner(scripts, next) {
            var ln = scripts.length;
            var count = 0;
            var isDone = function () {
                count += 1;
                if (count >= ln) {
                    next();
                }
            };
            scripts.forEach(function (script) {
                script(isDone);
            });
        }

        // 1.
        function setupExportFolders(next) {
            runner([addVeevaWideJS, updateModelJSON], next);
        }

        // 2.1
        function sharedScripts(next) {
            var scripts = [
                addSharedAssets,
                addPresentationData,
                addModulesAssets
            ];
            runner(scripts, next);
        }

        // 2.2
        function slideScripts(next) {
            runner([
                insertSlideHTML,
                addSlideAssets,
                addSlidePopupAssets
            ], next);
        }

        // 3.
        function processFiles(next) {
            runner([sharedScripts, slideScripts], next);
        }

        // 4.
        function fontInsert(next) {
            runner([addBuildStyles], next);
        }

        // 5.
        function updateReferences(next) {
            if (references) {
                runner([updateReferencesNumbers], next);
            }else{
                next();
            }
        }

        // 6.
        function restructureShared(next) {
            var scripts = [
                moveSharedPdfs,
                deleteRedundantFiles
            ];
            // if (cleanup) scripts.push(deleteNonPopupSlides);
            runner(scripts, next);
        }

        // 7.
        function finalizeExports(next) {
            var scripts = [
                updateSlideModel,
                addVendorScripts,
                addJsonFiles,
                addLibFiles
            ];
            if (config.thumbs) scripts.push(addSlideThumbs);
            runner(scripts, next);
        }

        setupExportFolders(function () {
            processFiles(function () {
                fontInsert(function () {
                    updateReferences(function () {
                        restructureShared(function () {
                            insertSharedFolder(function () { // 4.
                                finalExport(function () { // 5.
                                    finalizeExports(function () {
                                        console.log('Deleting tmp folder with files');
                                        del([dest])
                                            .then(function () {
                                                cb(null, destFolder);
                                            })
                                            .catch(function (err) {
                                                cb(err, destFolder);
                                            });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
    // });
};
