const fs = require('fs');
const Path = require('path');
const gulp = require('gulp');
const del = require('del');
const template = require('gulp-template');
const replace = require('gulp-replace');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');


module.exports = function (slides, config,next) {
    var path = config.path || 'app/slides/';
    var outputPath = config.output || 'exports';
    var prefix = config.prefix || '';

    var slidesPath = Path.resolve(process.cwd(), path);

    var jsonTemplate = require('../framework-presenatation.json');

    var startTime = Date.now();
    var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
    var dest = Path.resolve(outputPath, 'veeva-export-' + now);

    var buildSettings = require(process.cwd() + '/templates/veeva-wide-shared/build.json');

    var endTime, diffTime;
    var ln = slides.length;

    function copyFromTemplate() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(process.cwd() + '/templates/veeva-wide-slide/**/*')
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        addSharedFolder();
                    }
                });
        });
    }

    // TODO: run next four as parallell instead of serial
    function addSharedFolder() {
        gulp.src(buildSettings.copies)
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                addSharedJS();
            });
    }

    function addSharedJS() {
        gulp.src(buildSettings.sharedJS)
            .pipe(concat('bundle.js'))
            .pipe(uglify())
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                addSharedCSS();
            });
    }

    function addSharedCSS() {
        gulp.src(buildSettings.sharedCSS)
            .pipe(replace(/[\w-_\/\.]+(\/module_assets\/[\w-_\/]+\.(jpg|png|woff|otf))/g, '.$1'))
            .pipe(replace(/[\w-_\/\.]+(\/assets\/images\/[\w-_\/]+\.(jpg|png))/g, '.$1'))
            .pipe(replace(/[\w-_\/\.]+(\/thumbnails\/[\w-_\/]+\.(jpg|png))/g, '.$1'))
            .pipe(concat('bundle.css'))
            .pipe(cleanCSS())
            .pipe(gulp.dest(dest + '/shared'))
            .on('end', function() {
                getSlideFiles();
            });
    }

    function resolveDependencies(slide) {
        var local = require(Path.join(process.cwd(), 'app/slides/' + slide + '/dependencies.json'));
        var deps = ['build/agnitio/**/' + slide + '*'];
        if (local.popups) {
            local.popups.forEach(function(popup) {
                deps.push('build/agnitio/**/' + popup + '*');
            });
        }
        return deps;
    }

    function getSlideFiles() {
        var counter = 0;
        slides.map(function(slide) {
            var dependencies = resolveDependencies(slide);
            gulp.src(dependencies)
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        insertSlideHTML();
                    }
                });
        });
    }

    function insertSlideHTML() {
        var counter = 0;
        slides.map(function(slide) {
            var index = dest + '/' + prefix + slide + '/index.html';
            var src = dest + '/' + prefix + slide + '/slides/' + slide + '.html';
            fs.readFile(src, 'utf8', function(err, slideHtml) {
                if (!err) {
                    fs.readFile(index, 'utf8', function(err, indexHtml) {
                        var updated = indexHtml.replace(/<!-- slideHtml -->/, slideHtml);
                        fs.writeFile(index, updated, 'utf8', function(err) {
                            if (!err) {
                                counter += 1;
                                if (counter >= ln) {
                                    createStoryboard();
                                }
                            }
                            else {
                                next(err);
                            }
                        });
                    });
                }
                else{
                    // We just ignore then
                    counter += 1;
                    if (counter >= ln) {
                        createStoryboard();
                    }

                }
            });
        });
    }

    function createStoryboard() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*.json')
                .pipe(template({slideId: slide}))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        buildCSS();
                    }
                });
        });
    }

    function buildCSS() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*css')
                .pipe(concat('slide.css'))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        buildJS();
                    }
                });
        });
    }

    function buildJS() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*js')
                .pipe(concat('slide.js'))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        replacePaths();
                    }
                });
        });
    }

    // Replace local paths to point to shared folder
    function replacePaths() {
        var counter = 0;
        slides.map(function(slide) {
            gulp.src(dest + '/' + prefix + slide + '/**/*')
                .pipe(replace(/[\w-_\/\.]+(\/module_assets\/[\w-_\/]+\/images\/[\w-_\/]+\.(jpg|png))/g, '../shared$1'))
                .pipe(replace(/[\w-_\/\.]+(\/assets\/images\/[\w-_\/]+\.(jpg|png))/g, '../shared$1'))
                .pipe(replace(/[\w-_\/\.]+(\/thumbnails\/[\w-_\/]+\.(jpg|png))/g, '../shared$1'))
                .pipe(gulp.dest(dest + '/' + prefix + slide))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        endTime = Date.now();
                        diffTime = (endTime - startTime) / 1000;
                        console.log("Project exported to " + dest + " in " + diffTime + " seconds.");
                        next(null, dest);
                    }
                });
        });

    }

    copyFromTemplate();

}
