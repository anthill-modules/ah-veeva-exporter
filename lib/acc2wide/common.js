const fs = require('fs-extra');
const Path = require('path');
const cheerio = require('cheerio');

function getJsonData(json) {
	var src = process.cwd() + json.path + json.name;
	return fs.readJsonSync(src);
}

function updateSlidesData(slides, destFolder, ln) {
	return function(parseFunction, identifier, next) {
		var counter = 0;

		slides.map(function (slide) {
			var slideId = slide.id,
				slidePath = Path.join(destFolder, slideId, 'index.html');

			fs.readFile(slidePath, 'utf8', function (err, slideHtml) {
				if (!err) {
					const $ = cheerio.load(slideHtml, {_useHtmlParser2: true});
					fs.outputFile(slidePath, parseFunction($, slide).html(), 'utf8', function (err) {
						if (!err) {
							counter += 1;
							if (counter >= ln) {
								console.log('END: ' + identifier + ' updated!');
								next();
							}
						}
						else {
							console.log('END: ' + identifier + ' updated!');
							console.log(err);
							next(err);
						}
					});
				}
				else {
					// We just ignore then
					counter += 1;
					if (counter >= ln) {
						console.log(identifier + ' failed to update!');
						next();
					}

				}
			});
		});
	};
}

module.exports = {
	getJsonData: getJsonData,
	updateSlidesData: updateSlidesData
};