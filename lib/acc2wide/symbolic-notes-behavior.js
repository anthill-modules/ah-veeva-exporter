const defaultConfig = {
	map: ['*', '†', '‡', '§', '‖', '¶']
}

class NoteSymbol {
	constructor(config = defaultConfig) {
		const { map: { length } = [] } = config;
		this.config = config;
		this.mapLength = length;
	}

	getDiff(position) {
		return Math.floor((position - 1) / this.mapLength);
	}

	getIndicatorSymbol(position) {
		const target = position % this.mapLength;
		return this.config.map[target ? target - 1 : this.mapLength - 1];
	}

	repeatSymbol(symbol, times) {
		return new Array(times + 1).join(symbol);
	}

	getIndicator(position) {
		const times = this.getDiff(position);
		const indicator = this.getIndicatorSymbol(position);
		return indicator + this.repeatSymbol(indicator, times);
	}
}

module.exports = NoteSymbol;