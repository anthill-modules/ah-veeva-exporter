const commonModule = require('./common');
const NoteSymbol = require('./symbolic-notes-behavior');
const getJsonData = commonModule.getJsonData;

const noteSymbol = new NoteSymbol();

function updateNotesLetters(slides, destFolder, ln, separator, digitalNotes, notesType) {
	const updateSlidesData = commonModule.updateSlidesData(slides, destFolder, ln);

	return function(next) {
		console.log('START: Notes update');
		const notesList = getNotesList(),
			notesAttribute = 'data-notes';

		function parseNotes($) {
			const notesOnSlide = $('[' + notesAttribute + ']'),
				orderedNotesList = {};
			let number = 1;
			notesOnSlide.each((index, notesNode) => {
				const attr = $(notesNode).attr(notesAttribute);
				const notes = attr.split(' ').map((noteKey) => {
					if (!notesList[noteKey].isAbbreviation && !orderedNotesList[noteKey]) {
						orderedNotesList[noteKey] = number++;
					}
					return orderedNotesList[noteKey];
				})
					.filter(function (e) {
						return e
					}); //getting rid of empty values
				$(notesNode).text(joinNotes(notes, separator, getNotesIdentifierParser(digitalNotes, notesType)));
			});
			return $;
		}

		updateSlidesData(parseNotes, 'Notes', next);
	}
}

function getNotesList() {
	console.log('Get notes');
	return getJsonData({path: '/build/', name: 'notes-strings.json'});
}

function joinNotes(notes, separator, notesIdentifierCb) {
	const notesOrigin = notes;
	let generatedIdentifier = '',
		valueSeparator = '';
	return notes.map((currentValue, index, array) => {
		if (valueSeparator === separator) {
			generatedIdentifier = valueSeparator = '';
		}
		else {
			generatedIdentifier = getIdentifier(index, notesOrigin, notesIdentifierCb);
			valueSeparator = getSeparator(index, notesOrigin, separator);
		}
		return generatedIdentifier + valueSeparator;
	})
		.join('');
}

function getIdentifier(index, notes, notesIdentifierCb) {
	if (isLastElement(index, notes.length) || !elementIsTrimmed(notes[index - 1], notes[index], notes[index + 1])) {
		return notesIdentifierCb(notes[index]);
	} else {
		return '';
	}
}

function getNotesIdentifierParser(digitalNotes, notesType) {
	const startLetterCode = 64,
		latinAlphabetLength = 26;
	let parser = null;

	if (digitalNotes) {
		parser = (number) => {
			return number;
		}
	} else {
		switch (notesType) {
			case 'symbolic':
				parser = noteSymbol.getIndicator.bind(noteSymbol);
				break;
			default:
				parser = (number) => {
					return String.fromCharCode(startLetterCode + (number % latinAlphabetLength)).toLowerCase();
				}
		}
	}
	return parser;
}

function getSeparator(index, notes, separator) {
	const previousValue = notes[index - 1],
		currentValue = notes[index],
		nextValue = notes[index + 1];
	if (isLastElement(index, notes.length) || elementIsTrimmed(previousValue, currentValue, nextValue)) { //no separator is required if current element is last in the order or if it is trimmed
		return '';
	} else if (elementIsTrimmed(currentValue, nextValue, notes[index + 2])) { //check if next element is trimmed
		return separator;
	} else {
		return ',';
	}
}

function isLastElement(index, length) {
	return (index === length - 1)
}

function elementIsTrimmed(previousValue, currentValue, nextValue) {
	return previousValue && currentValue && nextValue && currentValue === previousValue + 1 && currentValue === nextValue - 1
}

module.exports = updateNotesLetters;