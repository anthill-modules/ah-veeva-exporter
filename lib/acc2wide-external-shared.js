const fs = require('fs-extra');
const Path = require('path');
const gulp = require('gulp');
const del = require('del');
const template = require('gulp-template');
const gulpif = require('gulp-if');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const merge = require('merge-stream');
const coffee = require('gulp-coffee');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const font2css = require('gulp-font2css').default;
const header = require('gulp-header');
const postcss = require('gulp-postcss');
const cssClean = require('postcss-clean');
const autoprefixer = require('autoprefixer');
const images = require('images');

const map = require('vinyl-map2');

const notesModule = require('./acc2wide/notes');
const referencesModule = require('./acc2wide/references');
const getSharedPath = require('./acc2wide/shared');
const addSitemapThumbs = require('./acc2wide/thumbnails');

// Utility functions
function buildCache(next) {
	console.log('Building cache file');
	var str = 'app.cache.put(\'package.json\',\'\')';
	var file = Path.join(process.cwd(), 'app/platforms/veeva/js/cache.js');
	fs.writeFile(file, str, function (err) {
		if (err) console.log('Error found writing media file:', err, __dirname);
		next(err);
	});
}

module.exports = function (slides, config, cb) {
	console.log('NUMBER OF SLIDES:', slides.length);
	buildCache(function (err) {
		var outputPath = config.output || 'exports';
		var prefix = config.prefix || '';
		var references = config.references || false; // Clean up slides not linked to
		var separator = config.separator || '-';
		var popup = config.popup || false;
		var engage = config.engage || false;
		var limited = config.limited || false; // Limit overview to structure
		var notes = config.notes || false;
		var pdfs = config.pdfs || false;
		var digitalNotes = config.digital || false;
		var localThumbs = config.localThumbs || false;
		var notesType = config.notesType || false;
		var veevaConfig = getJsonData({path: '/app/', name: 'config.json'});
		var sharedKMName = veevaConfig.veeva.shared_key_message.name || 'shared';

		var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
		var destFolder = Path.resolve(outputPath, 'veeva-export-' + now);
		var ln = slides.length;
		var presentationData = getPresentationData();
		var isFontsFilesExist = isFontsExist();
		var detectedPopups = {
			globalList: [],
			localLists: {}
		};
		var updateNotesLetters = notesModule(slides, destFolder, ln, separator, digitalNotes, notesType);
		var updateReferencesNumbers = referencesModule(slides, destFolder, ln, separator);
		var sharedPath = getSharedPath({isEngage: engage});
		function getPresentationData() {
			var src = Path.join(process.cwd(), 'build/presentation.json'),
				data = fs.readJsonSync(src);
			return data;
		}

		function addVeevaWideJS(next) {
			console.log('START: Add VW JS files');
			var src = process.cwd() + '/app/platforms/veeva/js/**/*';
			gulp.src(src)
				.pipe(gulp.dest(destFolder + '/' + sharedKMName))
				.on('end', function () {
					console.log('END: Add VW JS files');
					next();
				});
		}

		function addSharedAssets(next) {
			console.log('START: Add shared assets');
			var src = [process.cwd() + '/app/shared/**/*', '!' + process.cwd() + '/app/shared/images/icons/*', '!' + process.cwd() + '/app/shared/pdfs/*'];
			gulp.src(src)
				.pipe(gulp.dest(destFolder + '/' + sharedKMName + '/shared'))
				.on('end', function () {
					console.log('END: Add shared assets');
					next();
				});
		}

		function getJsonData(json) {
			var src = process.cwd() + json.path + json.name;
			return fs.readJsonSync(src);
		}

		function deleteFonts(next) {
			console.log('START: Delete fonts from styles');
			var src = Path.join(process.cwd(), 'build/build/presentation/styles.css');
			console.log(src);

			fs.readFile(src, 'utf8', function (err, data) {
				if (!err) {
					data = data.replace(/@font-face\s?\{[^}]*\}/g, '');
					fs.writeFile(src, data, 'utf8', function (err) {
						if (!err) {
							console.log('END: Delete fonts from styles');
							next();
						}
						else {
							next(err);
						}
					});
				}
			});
		}

		function addFonts(next) {
			console.log('START: Concating font');
			var src = Path.join(process.cwd(), 'build/build/presentation/styles.css');
			var fonts = destFolder + '/shared/global_styles/assets/fonts.css';
			var destC = Path.join(process.cwd(), 'build/build/presentation');
			if (!isFontsFilesExist) {
				next();
				return;
			}
			gulp.src(src)
				.pipe(header(fs.readFileSync(fonts), 'utf8'))
				.pipe(gulp.dest(destC))
				.on('end', function () {
					del(fonts)
					console.log('END: Concat font [delete fonts.css]');
					next();
				});
		}

		function isFontsExist() {
			console.log('Checking is fonts files exist in presentation');
			var pathToFonts = process.cwd() + '/build/global_styles/assets/fonts';
			return fs.existsSync(pathToFonts) && fs.readdirSync(pathToFonts).length > 0;
		}

		function fonts2css(next) {
			console.log('START: Convert fonts to css');
			var pathToStyles = process.cwd() + '/build/global_styles/assets/**/*';
			gulp.src(pathToStyles)
				.pipe(font2css())
				.pipe(concat('fonts.css'))
				.pipe(gulp.dest(destFolder + '/' + sharedKMName + '/global_styles/assets'))
				.on('end', function () {
					console.log('END: Convert fonts to css');
					next();
				});
		}

		function addBuildStyles(next) {
			console.log('START: Add build styles');
			var pathToStyles = process.cwd() + '/build/build/presentation/**/*';
			gulp.src(pathToStyles)
				.pipe(gulp.dest(destFolder + '/' + sharedKMName + '/build/presentation'))
				.on('end', function () {
					console.log('END: Add build styles');
					next();
				});
		}

		function addModulesAssets(next) {
			console.log('START: Add modules assets');
			var pathToStyles = process.cwd() + '/build/modules/**/*.{png,jpg,jpeg}';
			gulp.src(pathToStyles)
				.pipe(gulp.dest(destFolder + '/' + sharedKMName + '/modules'))
				.on('end', function () {
					console.log('END: Add modules assets');
					next();
				});
		}

		// Writes to final export (destFolder)
		function updateSlideModel(next) {
			console.log('START: Update slide model');
			var src = Path.join(process.cwd(), 'build/presentation.json');
			var counter = 0;
			fs.readJson(src, 'utf8', function (err, model) {
				if (err) {
					next(err);
				}
				Object.keys(model.storyboards).forEach(function (sbId) {
					delete model.storyboards[sbId].linear;
					delete model.storyboards[sbId].type;
					//delete model.storyboards[sbId].country;
					delete model.storyboards[sbId].start;
				});
				Object.keys(model.structures).forEach(function (structureId) {
					delete model.structures[structureId].type;
					delete model.structures[structureId].shareable;
				});
				Object.keys(model.modules).forEach(function (moduleId) {
					delete model.modules[moduleId].version;
					delete model.modules[moduleId].description;
					delete model.modules[moduleId].type;
				});

				Object.keys(model.slides).forEach(function (slideId) {
					delete model.slides[slideId].files.scripts;
					delete model.slides[slideId].files.styles;
					delete model.slides[slideId].country;
				});

				slides.map(function (slide) {
					var localModel = JSON.parse(JSON.stringify(model)),
						indexPath = Path.join(destFolder, slide.id, 'index.html');

					// eg Xarelto, change structure to have single slide
					if (!prefix || limited) {
						localModel.structures[slide.structure].content = [slide.name];
						localModel.storyboards[slide.storyboard].content = [slide.structure];
					}
					// eg Mirena, change storyboard to have single chapter (that has single slide)
					else {
						localModel.structures.overview = {
							'name': 'Overview',
							'id': 'overview',
							'type': 'slideshow',
							'content': localModel.storyboards[slide.storyboard].content.slice()
						};
						localModel.storyboards[slide.storyboard].content = [slide.structure];
					}

					fs.readFile(indexPath, 'utf8', function (err, indexHtml) {
						if (!err) {
							var updated = indexHtml.replace(/'model.json'/g, JSON.stringify(localModel));
							fs.outputFile(indexPath, updated, function (err) {
								if (!err) {
									counter += 1;
									if (counter >= ln) {
										console.log('END: Update slide model');
										next();
									}
								}
								else {
									console.log('END: Update slide model');
									next(err);
								}
							});
						} else {
							next(err);
						}
					});
				});
			});
		}

		function getPopups(slide, path) {
			var localPopups = getSlideDetectedPopups(detectedPopups.localLists[slide.name]);
			return popup ? 'slides/*' + '(' + detectedPopups.globalList + '|' + localPopups + ')' + path : 'slides/**PopupSlide' + path;
		}

		function getPopupList(slide) {
			return detectedPopups.globalList.concat(detectedPopups.localLists[slide.name]);
		}

		function getSlideDetectedPopups(popupsList) {
			if (typeof popupsList !== 'undefined') {
				return popupsList.join('|');
			} else {
				return '';
			}
		}

		function getPopupsGlobalList() {
			console.log('Get list of global popups');
			var popups = Object.keys(presentationData.slides).filter(function (slideId) {
				return presentationData.slides[slideId]['isGlobal'];
			});
			return (Array.isArray(popups)) ? popups : [];
		}

		function getPopupsFromSlideHtml(slideName) {
			console.log('Get popups from ' + slideName + ' html');
			var src = Path.join(process.cwd(), 'build', 'slides', slideName, slideName + '.html'),
				targetPopup = /data(?:-.+){0,}-popup(?:-.+){0,}\s*=\s*['"](.+popupslide)(?=['"])/gi,
				targetInlineSlideshow = /(data-viewer=['"](?!browser)[^>]+href|data-inline-slideshow)=['"](.+?)(?=['"])/gi,
				dataFromHTML = fs.readFileSync(src, 'utf8'),
				result = [];

			[
				{
					match: targetInlineSlideshow,
					slideshow: presentationData.structures
				},
				{
					match: targetPopup
				}
			].forEach(function (searchItem) {
				var match;
				while (match = searchItem.match.exec(dataFromHTML)) {
					if (searchItem.slideshow) {
						searchItem.slideshow[match[2]].content.forEach(function (inlineSlide) {
							result.push(inlineSlide);
						});
					} else {
						result.push(match[1]);
					}
				}
			});
			return (Array.isArray(result)) ? result : [];
		}

		function getPopupsFromSlideScripts(slideName) {
			console.log('Get popups from ' + slideName + ' script');
			var src = Path.join(process.cwd(), 'build/slides', slideName),
				filesMask = ['.js', '.coffee'],
				target = /popupsToDetect\s*=\s*(\[(?:[^\]]|\n)*\])/mi,
				popups = [],
				srcFile,
				match;

			filesMask
				.map(function (mask) {
					return Path.join(src, slideName + mask);
				})
				.forEach(function (file) {
					srcFile = fs.pathExistsSync(file);
					if (srcFile) {
						content = fs.readFileSync(file, 'utf8');
						match = content.match(target);
						if (match) {
							popups = popups.concat(JSON.parse(match[1]));
						}
					}
				});
				return popups;
		}

		function deepSearchPopups(slideName, callbacks) {
			var popupsFullList = [];
			(function deepSearchRecursion(slideName, callbacks) {
				if (!popupsFullList.includes(slideName)) {
					callbacks.forEach(function (callback) {
						callback(slideName).forEach(function (slide) {
							popupsFullList.push(slide);
							deepSearchRecursion(slide, callbacks);
						});
					});
				}
			})(slideName, callbacks);
			console.log(slideName, 'popupsFullList is', popupsFullList);
			return popupsFullList;
		}

		function addDetectedPopups(next) {
			detectedPopups.globalList = getSlideDetectedPopups(getPopupsGlobalList()) || [];

			slides.forEach(function (slide) {
				detectedPopups.localLists[slide.name] = [];
				var uniqueResults = {};

				deepSearchPopups(slide.name, [getPopupsFromSlideHtml, getPopupsFromSlideScripts]).forEach(function (el) {
					uniqueResults[el] = true;
				});
				detectedPopups.localLists[slide.name] = Object.keys(uniqueResults);
			});
			next();
		}

		function buildPopupsHTMLEngage(slide) {
			var popupHTML = '';
			var path = '';
			var html = '';
			getPopupList(slide).forEach(function(item) {
				if(item.trim()) {
					path = Path.join(process.cwd(), 'build/slides', item, item + '.html');
					html = fs.readFileSync(path, 'utf8');
					html = html.replace(item, item + '-template');
					html = html.replace(/assets\/images/g, 'slides/' + item + '/assets/images');
					popupHTML += html;
				}
			});

			return popupHTML;
		}

		function insertSlideHTML(next) {
			console.log('START: Insert slide HTML');
			var counter = 0;
			var index = Path.join(process.cwd(), 'build', 'index.html');
			var head = Path.join(process.cwd(), 'app/platforms/veeva', 'head.html');
			var bottom = Path.join(process.cwd(), 'app/platforms/veeva', 'bottom.html');

			fs.readFile(index, 'utf8', function (err, indexHtml) {
				fs.readFile(head, 'utf8', function (err, headHtml) {
					fs.readFile(bottom, 'utf8', function (err, bottomHtml) {
						slides.map(function (slide) {
							// console.log(slide);
							var slideId = slide.id;
							if (prefix) slideId = prefix + slide.name;
							if (engage) {
								var popupHTML = buildPopupsHTMLEngage(slide);
							}
							var destPath = Path.join(destFolder, slideId, 'index.html');
							var src = Path.join(process.cwd(), 'build', 'slides', slide.name, slide.name + '.html');
							var slideAssetsPath = '"slides/' + slide.name + '/assets/';
							fs.readFile(src, 'utf8', function (err, slideHtml) {
								if (!err) {
									// Fix accelerator events and classes
									var updated = indexHtml.replace(/<!--slideHtml-->/, slideHtml);
									if (engage) {
										updated = updated.replace(/<!--popupHTML-->/g, popupHTML);
									}
									updated = updated.replace('class="slide"', 'class="slide present"');
									updated = updated.replace('class="slide ', 'class="slide present ');
									updated = updated.replace(/<!--headHTML-->/g, headHtml);
									updated = updated.replace(/<!--bottomHTML-->/g, bottomHtml);
									updated = updated.replace(/<%= slide %>/g, slide.name);
									updated = updated.replace(/<%= slideshow %>/g, slide.structure);
									updated = updated.replace(/%--slideshow--%/g, slide.structure);
									updated = updated.replace(/%--typeBuild--%/g, engage ? 'veeva-wide veeva-engage' : 'veeva-wide');
									updated = updated.replace(/<%= storyboard %>/g, slide.storyboard);
									updated = updated.replace(/%--storyboard--%/g, slide.storyboard);
									updated = updated.replace(/<%= prefix %>/g, prefix);
									updated = updated.replace(/"assets\//g, slideAssetsPath);
									//delete libs
									updated = updated.replace(/<script src="accelerator\/lib\/agnitio.js"><\/script>/g, '');
									//fix libs
									updated = updated.replace(/<!--<script src="accelerator\/lib\/touchy.min.js"><\/script>-->/g, '<script src="accelerator/lib/touchy.min.js"></script>');
									updated = updated.replace(/<!--<script src="accelerator\/lib\/touchy.js"><\/script>-->/g, '<script src="accelerator/lib/touchy.js"></script>');
									//update shared assets URI
									updated = updated.replace(/"shared\/images/g, '"' + sharedPath + '/shared/images');
									updated = updated.replace(/\.\/\.\/shared\/images/g, sharedPath + '/shared/images');
									updated = updated.replace(/"shared\/videos/g, '"' + sharedPath + '/shared/videos');
									updated = updated.replace(/\.\/\.\/shared\/videos/g, sharedPath + '/shared/videos');
									if(pdfs){
										updated = updated.replace(/"shared\/pdfs/g, '"' + sharedPath + '/shared/pdfs');
										updated = updated.replace(/\.\/\.\/shared\/pdfs/g, sharedPath + '/shared/pdfs');
									}
									updated = updated.replace(/build\//g, sharedPath + '/build/');
									updated = updated.replace(/accelerator\//g, sharedPath + '/accelerator/');
									updated = updated.replace(/notes.js/g, sharedPath + '/notes.js');
									updated = updated.replace(/cache.js/g, sharedPath + '/cache.js');

									fs.outputFile(destPath, updated, 'utf8', function (err) {
										if (!err) {
											counter += 1;
											if (counter >= ln) {
												console.log('END: Insert slide HTML');
												next();
											}
										}
										else {
											console.log(err);
											console.log('END: Insert slide HTML');
											next(err);
										}
									});
								}
								else {
									// We just ignore then
									counter += 1;
									if (counter >= ln) {
										console.log('END: Insert slide HTML');
										next();
									}

								}
							});
						});
					});
				});
			});
		}

		function addSlideAssets(next) {
			console.log('START: Add slide assets');
			var counter = 0;
			slides.map(function (slide) {
				var folder = Path.join(destFolder, slide.id, 'slides', slide.name, 'assets');
				var src = Path.join(process.cwd(), 'build', 'slides', slide.name, 'assets/**/*');

				gulp.src(src)
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Add slide assets');
							next(null, destFolder);
						}
					});
			});
		}

		function addSlidePopupsAssets(next) {
			console.log('START: Add slide popups assets');
			var counter = 0;
			slides.map(function (slide) {
				var folder = Path.join(destFolder, slide.id, 'slides');
				var popupsSrc = getPopups(slide, '/assets/**/*');
				var src = [
					Path.join(process.cwd(), 'build', popupsSrc),
					Path.join(process.cwd(), 'build', 'slides/**AlternativeSlide/assets/**/*')
				];

				gulp.src(src)
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Add slide popups assets');
							next(null, destFolder);
						}
					});
			});
		}

		function addSlidePopupsHTMLs(next) {
			console.log('START: Add slide popups HTMLs');
			var counter = 0;
			slides.map(function (slide) {
				var folder = Path.join(destFolder, slide.id, 'slides');
				var popupsSrc = getPopups(slide, '/*.html');
				var src = [
					Path.join(process.cwd(), 'build', popupsSrc),
					Path.join(process.cwd(), 'build', 'slides/**AlternativeSlide/*.html')
				];
				var minify = map(function (code, filename) {
					var id = Path.basename(filename, '.html');
					code = code.toString();
					code = code.replace(/assets\/images/g, 'slides/' + id + '/assets/images');
					return code;
				});

				gulp.src(src)
					.pipe(minify)
					.pipe(replace(/(\.\/\.\/)*shared\/images/g, sharedPath + '/shared/images'))
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Add slide popups HTMLs');
							next(null, destFolder);
						}
					});
			});
		}

		function copySlideStyles(next) {
			console.log('START: Copy Slides CSS');
			var counter = 0;
			slides.map(function (slide) {
				var popupsSrc = getPopups(slide, '/*.css');
				var folder = Path.join(destFolder, slide.id),
					src = [
						Path.join(process.cwd(), 'build', 'slides/', slide.name, slide.name + '.css'),
						Path.join(process.cwd(), 'build', popupsSrc),
						Path.join(process.cwd(), 'build', 'slides/**AlternativeSlide/*.css')
					];

				var minify = map(function (code, filename) {
					var id = Path.basename(filename, '.css');
					code = code.toString();
					code = code.replace(/assets\/images/g, 'slides/' + id + '/assets/images');
					return code;
				});

				gulp.src(src)
					.pipe(minify)
					.pipe(concat('slides.css'))
					.pipe(postcss([autoprefixer(), cssClean()]))
					.pipe(replace('../../shared/images', sharedPath + '/shared/images'))
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Copy Slides CSS');
							next(null, destFolder);
						}
					});
			});
		}

		function copySlideScripts(next) {
			console.log('START: Copy Slides JS');
			var counter = 0;
			slides.map(function (slide) {
				var popupsSrc = getPopups(slide, '/*.coffee');
				var folder = Path.join(destFolder, slide.id),
					src = [
						Path.join(process.cwd(), 'build', 'slides/', slide.name, slide.name + '.coffee'),
						Path.join(process.cwd(), 'build', popupsSrc),
						Path.join(process.cwd(), 'build', 'slides/**AlternativeSlide/*.coffee')
					];

				gulp.src(src)
					.pipe(coffee())
					.pipe(concat('slides.js'))
					.pipe(uglify())
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Copy Slides JS');
							next(null, destFolder);
						}
					});
			});
		}

		function moveSharedPdfs(next) {
			console.log('START: Move shared PDFs');
			var src = process.cwd() + '/build/shared/pdfs/**/*.pdf';
			var folder = pdfs ? Path.join(destFolder, sharedKMName, 'shared', 'pdfs') : Path.resolve(destFolder, '../shared_pdfs');
			del([folder]).then(function () {
				gulp.src(src)
					.pipe(gulp.dest(folder))
					.on('end', function () {
						console.log('END: Move shared PDFs');
						next(null, destFolder);
					});
			});
		}

		function addSlideThumbs(next) {
			console.log('START: Add slide thumbs');
			var counter = 0;
			slides.map(function (slide) {
				var folder = Path.join(destFolder, slide.id);
				var src = Path.join(process.cwd(), 'app/slides', slide.name, 'thumbnails/full-veeva.png');
				gulp.src(src)
					.pipe(rename('thumb.png'))
					.pipe(gulp.dest(folder))
					.on('end', function () {
						counter += 1;
						if (counter >= ln) {
							console.log('END: Add slide thumbs');
							next(null, destFolder);
						}
					});
			});
		}

		function addSharedThumb(next) {
			console.log('START: Add shared thumb');
			var folder = Path.join(destFolder, sharedKMName + '/');
			images(1024, 768)
				.fill(255, 255, 255, 1)
				.saveAsync(folder + 'thumb.png', {quality: 20}, '', function (data) {
					console.log('END: Add shared thumb');
					next(null, destFolder);
				});
		}

		function addVendorScripts(next) {
			console.log('START: Add vendor files');
			var src = Path.join(process.cwd(), 'build/_vendor/**/*');
			var folder = Path.join(destFolder, sharedKMName, '_vendor');
			gulp.src(src)
				.pipe(gulpif(/^((?!(min\.js)|css).)*$/, uglify()))
				.pipe(gulp.dest(folder))
				.on('end', function () {
					console.log('END: Add vendor files');
					next();
				});
		}

		function addLibFiles(next) {
			console.log('START: Add lib files');
			var src = Path.join(process.cwd(), 'build/accelerator/lib/**/*');
			var folder = Path.join(destFolder, sharedKMName, 'accelerator/lib');

			gulp.src(src)
				.pipe(gulpif(/^((?!(min\.js)|css).)*$/, uglify()))
				.pipe(gulp.dest(folder))
				.on('end', function () {
					console.log('END: Add lib files');
					next();
				});
		}

		function addJsonFilesShared(next) {
			console.log('Add JSON files SHARED');
			var folder = Path.join(destFolder, sharedKMName);
			// var src = Path.join(process.cwd(), 'build', 'slides', slide.name, '**/*.png');
			var src = [];
			src.push(Path.join(process.cwd(), 'build', '*.json'));
			src.push('!' + Path.join(process.cwd(), 'build', '{presentation,*-strings,config}.json'));

			gulp.src(src)
				.pipe(gulp.dest(folder))
				.on('end', function () {
					next();
				});

		}

		function saveSitemapThumbs(src, next) {
			var folder = Path.join(destFolder, sharedKMName, 'shared/thumbnails');
			gulp.src(src)
				.pipe(gulp.dest(folder))
				.on('end', function () {
					console.log('END: Add thumb Sitemap');
					next();
				});
		}

		function runAddingSitemapThumbs(next) {
			if (localThumbs) {
				addSitemapThumbs(next, saveSitemapThumbs);
			} else {
				next();
			}
		}

		function runner(scripts, next) {
			var ln = scripts.length;
			var count = 0;
			var isDone = function () {
				count += 1;
				if (count >= ln) {
					next();
				}
			};
			scripts.forEach(function (script) {
				script(isDone);
			});
		}

		// 1.
		function setupExportFolders(next) {
			runner([addVeevaWideJS], next);
		}

		// 2.
		function fontsFiles(next) {
			if (isFontsFilesExist) {
				runner([fonts2css, deleteFonts], next);
			} else {
				next();
			}

		}

		// 3.1
		function sharedScripts(next) {
			var scripts = [
				addSharedAssets,
				addSharedThumb,
				addFonts,
				addModulesAssets,
				addVendorScripts,
				addJsonFilesShared,
				addLibFiles
			];
			runner(scripts, next);
		}

		//3.3
		function popupScripts(next) {
			if (popup) {
				runner([addDetectedPopups], next);
			} else {
				next();
			}
		}

		// 3.2
		function slideScripts(next) {
			runner([
				insertSlideHTML,
				addSlideAssets,
				addSlidePopupsAssets,
				addSlidePopupsHTMLs,
				copySlideScripts,
				copySlideStyles
			], next);
		}

		// 3.
		function processFiles(next) {
			runner([sharedScripts, popupScripts, slideScripts], next);
		}

		// 4.
		function fontInsert(next) {
			runner([addBuildStyles], next);
		}

		// 5.
		function updateNotes(next) {
			if (notes) {
				runner([updateNotesLetters], next);
			} else {
				next();
			}
		}

		// 6.
		function updateReferences(next) {
			if (references) {
				runner([updateReferencesNumbers], next);
			} else {
				next();
			}
		}

		// 7.
		function restructureShared(next) {
			var scripts = [
				moveSharedPdfs
			];
			runner(scripts, next);
		}

		// 8.
		function finalizeExports(next) {
			var scripts = [
				updateSlideModel,
				runAddingSitemapThumbs
			];
			if (config.thumbs) scripts.push(addSlideThumbs);
			runner(scripts, next);
		}

		setupExportFolders(function () {
			fontsFiles(function () {
				processFiles(function () {
					fontInsert(function () {
						updateNotes(function () {
							updateReferences(function () {
								restructureShared(function () {
									finalizeExports(function () {
										cb(null, destFolder);
									});
								});
							});
						});
					});
				});
			});
		});
	});
	// });
};
